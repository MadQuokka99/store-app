import React from "react";

import "semantic-ui-css/semantic.min.css";
import { Routes, Route, BrowserRouter as Router } from "react-router-dom";

import HomePage from "./Pages/HomePage";
import MenuComponent from "./Components/MenuComponent";
import NotFoundPage from "./Pages/NotFoundPage";
import ContactPage from "./Pages/ContactPage";
import AboutPage from "./Pages/AboutPage";

function App() {
  return (
    <>
      <Router>
        <MenuComponent />
        <Routes>
          <Route path="/" exact element={<HomePage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
