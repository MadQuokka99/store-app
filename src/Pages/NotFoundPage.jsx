import React from "react";
import { Button, Grid } from "semantic-ui-react";
import { useNavigate } from "react-router-dom";
import TitleComponent from "../Components/TitleComponent";

const NotFoundPage = () => {
  const navigate = useNavigate();

  const handleGoToProductsPageButtonClicked = (e) => {
    navigate("/");
  };

  return (
    <Grid centered stackable>
      <Grid.Row>
        <TitleComponent pageTitle="The page was not found!" />
      </Grid.Row>
      <Grid.Row>
        <Button onClick={handleGoToProductsPageButtonClicked} color={"teal"}>
          Go to home page! 👀
        </Button>
      </Grid.Row>
    </Grid>
  );
};

export default NotFoundPage;
