import React, { useState, useEffect, useCallback } from "react";
import { productsListGet } from "../Utils/API-calls";
import { Button, Grid, Icon, Label, Message, Segment } from "semantic-ui-react";
import ProductListItemComponent from "../Components/ProductListItemComponent";
import ErrorComponent from "../Components/ErrorComponent";
import LoaderComponent from "../Components/LoaderComponent";
import ProductItem from "../Components/ProductItem";
import CartItem from "../Components/CartItem";

const HomePage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const [products, setProducts] = useState([]);

  const [productItemClicked, setProductItemClicked] = useState(null);
  const [cart, setCart] = useState([]);
  const [cartTotal, setCartTotal] = useState(0);

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    setError(null);

    try {
      const products = await productsListGet();

      setProducts(products.data);
      setIsLoading(false);
    } catch (error) {
      console.log(error.response);
      setError("Something went wrong! 🧯");
      setIsLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleProductItemClicked = (product) => {
    setProductItemClicked(product);
  };

  const handleBackToShopButtonClicked = (e) => {
    setProductItemClicked(null);
  };

  const handleAddToCartButtonClicked = (product) => {
    const cartUpdated = [...cart, product];
    setCart(cartUpdated);

    let cartTotalCopy = cartTotal;
    cartTotalCopy = cartTotal + product.price;
    setCartTotal(cartTotalCopy);
  };

  let cartSegment = (
    <Segment>
      <Icon name="cart" />{" "}
      {`Cart ${cart.length !== 0 ? `(${cart.length})` : ""}`}
      {cart.length === 0 ? (
        <Message info content="Cart is empty!" />
      ) : (
        cart.map((product) => <CartItem product={product} key={product.id} />)
      )}
      <Label color={cartTotal !== 0 ? "green" : null}>
        Total: ${cartTotal}
      </Label>
    </Segment>
  );

  let contentToBeShown = (
    <Grid columns={3} doubling verticalAlign="middle" centered>
      {products.map((product) => (
        <Grid.Column key={product.id}>
          <ProductListItemComponent
            product={product}
            key={product.id}
            handleProductItemClicked={handleProductItemClicked}
          />
        </Grid.Column>
      ))}
    </Grid>
  );

  if (productItemClicked) {
    contentToBeShown = (
      <ProductItem
        product={productItemClicked}
        handleAddToCartButtonClicked={handleAddToCartButtonClicked}
        cart={cart}
      />
    );
  }

  if (error) {
    contentToBeShown = <ErrorComponent errorMessage={error} />;
  }

  if (isLoading) {
    contentToBeShown = <LoaderComponent />;
  }

  return (
    <Grid style={{ margin: "1em" }} stackable>
      <Grid.Row columns={2}>
        <Grid.Column width={4}>
          {cartSegment}
          {productItemClicked && (
            <Button onClick={handleBackToShopButtonClicked}>
              Back to shop
            </Button>
          )}
        </Grid.Column>
        <Grid.Column width={12}>{contentToBeShown}</Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default HomePage;
