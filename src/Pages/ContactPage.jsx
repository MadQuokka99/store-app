import React from "react";
import SegmentComponent from "../Components/SegmentComponent";

const AboutPage = () => {
  return (
    <SegmentComponent
      title="Contact"
      content={
        <div style={{ textAlign: "center" }}>
          <p>droid@droid.com</p>
          <p>tel. 123 - 456 - 789 </p>
          <p>tel. 887 - 236 - 324</p>
          <p>Cluj, Cluj-Napoca </p>
          <p>Romania</p>
        </div>
      }
    />
  );
};

export default AboutPage;
