import Axios from "axios";
const { setupCache } = require("axios-cache-interceptor");

const axios = setupCache(Axios, {
  cache: console.log,
});

export const productsListGet = () => {
  return axios.get("https://fakestoreapi.com/products", {
    cache: {
      ttl: 180000,
    },
  });
};
