import React from "react";
import { Icon, Header, Grid } from "semantic-ui-react";
import PropTypes from "prop-types";
import "../Design/AboutPage.css";

const SegmentComponent = (props) => {
  return (
    <Grid className="about-page-grid">
      <Grid.Row centered>
        <Header as="h2" icon>
          <Icon name="address card" size="huge" />
          {props.title}
        </Header>
      </Grid.Row>
      <Grid.Row className="text-segment" centered>
        {props.content}
      </Grid.Row>
    </Grid>
  );
};

export default SegmentComponent;

SegmentComponent.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
};
