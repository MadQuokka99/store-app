import React, { useCallback, useEffect, useState } from "react";
import { Segment, Image, Header, Button } from "semantic-ui-react";
import PropTypes from "prop-types";

import "../Design/ProductComponent.css";

const ProductItem = (props) => {
  const [product, setProduct] = useState({});
  const [productIsAddedToCart, setProductIsAddedToCart] = useState(false);

  const isAddedToCart = useCallback(
    (product) => {
      return props.cart.filter((prod) => prod.id === product.id).length !== 0;
    },
    [props.cart]
  );

  useEffect(() => {
    setProduct(props.product);

    if (props.cart.length !== 0) {
      const isAdded = isAddedToCart(props.product);

      setProductIsAddedToCart(isAdded);
    }
  }, [props.product, props.cart, isAddedToCart]);

  const handleAddToCartButtonClicked = (e) => {
    props.handleAddToCartButtonClicked(product);
  };

  return (
    <Segment>
      <Image src={product.image} className="center-image" />
      <Header as="h1">{product.title}</Header>
      <p>{product.description}</p>
      <Button
        positive
        onClick={handleAddToCartButtonClicked}
        disabled={productIsAddedToCart}
      >
        {productIsAddedToCart ? "Added to cart" : "Add to cart"}
      </Button>
    </Segment>
  );
};

export default ProductItem;

ProductItem.propTypes = {
  product: PropTypes.object,
  cart: PropTypes.array,
};
