import React from "react";
import { Message, Label } from "semantic-ui-react";
import PropTypes from "prop-types";

const CartItem = (props) => {
  return (
    <Message key={props.product.id}>
      <Label color="green">{`$${props.product.price}`}</Label>{" "}
      {props.product.title}
    </Message>
  );
};

export default CartItem;

CartItem.propTypes = {
  product: PropTypes.object,
};
