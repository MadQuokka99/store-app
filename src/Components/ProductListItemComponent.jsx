import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Card, Image, Rating } from "semantic-ui-react";

import "../Design/ProductComponent.css";

const ProductItemComponent = (props) => {
  const [product, setProduct] = useState({});

  useEffect(() => {
    setProduct(props.product);
  }, [props.product]);

  const handleProductItemClicked = (e) => {
    props.handleProductItemClicked(product);
  };

  return Object.keys(product).length === 0 ? null : (
    <Card onClick={handleProductItemClicked}>
      <Image src={product.image} className="center-image" />
      <Card.Content>
        <Card.Meta>
          <span>{`$${product.price}`}</span>
        </Card.Meta>
        <Card.Description>{product.title}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Rating
          icon="star"
          defaultRating={product.rating.rate}
          maxRating={5}
          disabled
        />
        {` (${product.rating.rate})`}
      </Card.Content>
    </Card>
  );
};

export default ProductItemComponent;

ProductItemComponent.propTypes = {
  product: PropTypes.object,
};
