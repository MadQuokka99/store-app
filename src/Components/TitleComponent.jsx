import propTypes from "prop-types";
import React from "react";
import { Header } from "semantic-ui-react";
import "../Design/GeneralElements.css";

const TitleComponent = (props) => {
  return (
    <div className="message-info">
      <Header as="h1">{props.pageTitle}</Header>
    </div>
  );
};

export default TitleComponent;

TitleComponent.propTypes = {
  pageTitle: propTypes.string.isRequired,
};
