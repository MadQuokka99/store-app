import React from "react";
import PropTypes from "prop-types";
import { Message } from "semantic-ui-react";
import "../Design/GeneralElements.css";

const ErrorComponent = (props) => {
  return (
    <div className="center-div">
      <Message negative size="big">
        {props.errorMessage}
      </Message>
    </div>
  );
};

export default ErrorComponent;

ErrorComponent.propTypes = {
  errorMessage: PropTypes.string.isRequired,
};
