import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Menu, Image } from "semantic-ui-react";

const MenuComponent = () => {
  const [activeMenuItem, setActiveMenuItem] = useState("home");

  let navigate = useNavigate();

  const handleMenuItemClick = (e, { name }) => {
    setActiveMenuItem(name);
    if (name === "home" || name === "logo") {
      navigate("/");
    } else {
      navigate(`/${name}`);
    }
  };

  return (
    <Menu secondary>
      <Menu.Item name="logo" onClick={handleMenuItemClick}>
        <Image src="https://img.icons8.com/external-icongeek26-linear-colour-icongeek26/64/000000/external-clothes-donation-and-charity-icongeek26-linear-colour-icongeek26.png" />
      </Menu.Item>
      <Menu.Item
        name="home"
        active={activeMenuItem === "home"}
        onClick={handleMenuItemClick}
      />
      <Menu.Item
        name="about"
        active={activeMenuItem === "about"}
        onClick={handleMenuItemClick}
      />
      <Menu.Item
        name={"contact"}
        active={activeMenuItem === "contact"}
        onClick={handleMenuItemClick}
      />
    </Menu>
  );
};

export default MenuComponent;
