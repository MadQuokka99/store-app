import React from "react";
import { Loader } from "semantic-ui-react";
import "../Design/GeneralElements.css";

const LoaderComponent = () => {
  return (
    <div className="center-div">
      <Loader active inline="centered" size="large">
        Loading...
      </Loader>
    </div>
  );
};

export default LoaderComponent;
