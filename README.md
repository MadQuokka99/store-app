# Store App

This project allows users to see the list of products from the store. Within the project they can select a certain product to view its specifications and can add the product into the cart.

The project was created with **ReactJS** and **JavaScript**.

Used **node packages**:
* [axios](https://www.npmjs.com/package/axios)
* [axios-cache-interceptor](https://axios-cache-interceptor.js.org/#/)
* [semantic-ui-react](https://react.semantic-ui.com/)
* [react-router-dom](https://v5.reactrouter.com/web/guides/quick-start)
* [prop-types](https://www.npmjs.com/package/prop-types)

Data source: [Fake Store API](https://fakestoreapi.com/)

## Table of contents
1. [Installation](https://gitlab.com/MadQuokka99/store-app#installation)
2. [Usage](https://gitlab.com/MadQuokka99/store-app#usage)
3. [Roadmap](https://gitlab.com/MadQuokka99/store-app#roadmap)

## Installation

After the project was cloned, you have to run these scripts in the project directory (it requires to have installed [Node.js](https://nodejs.org/en/)):

```bash
npm install
```
Installs the Node Package Manager (NPM).

```bash
npm start
```
Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Usage

1. Home Page

Within this page users can scroll through the displayed products. They can click on a certain product in order to see more details.

<img src="src/readme-images/home-page.jpg" width="700" />

Users can add products to cart by clicking on the **Add to cart** button. Once a product is added to the cart, it cannot be added again. 

<img src="src/readme-images/product.jpg" width="600" />

The cart is updating its content while the user is adding products in it. The total price and the number of items in the cart are also updating.


<img src="src/readme-images/cart.jpg" width="600" />

2. About Page

In this page users can read a short description about the store.

<img src="src/readme-images/about-page.jpg" width="400" />

3. Contact Page

Within this page users can find out contact details of the store.

<img src="src/readme-images/contact-page.jpg" width="400" />

## Roadmap

* The application can be improved by using Redux to better manage the state
* Also, there could have been created more custom Hooks to improve the performance and readability of the code
* Another important aspect to be completed in the future would be to implement unit testing 



